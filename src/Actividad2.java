import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Actividad2 {
    public static void main(String[] args) {
        /**
         * Java no puede ejecutarse, se tiene que revisar el concepto∫
         */
        String command = "java src/Ramdom10.java";
        List<String> arglist = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(arglist);
        pb.inheritIO();
        try {
            Process process = pb.start();
            //Si se hace esto hay que esperar al proceso. 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
