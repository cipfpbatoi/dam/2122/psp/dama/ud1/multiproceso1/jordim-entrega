import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Actividad3 {
    public static void main(String[] args) {
        /**
         * Se tiene que revisar por completo
         */
        String command = "java src/Minusculas.java";
        List<String> arglist = new ArrayList<>(Arrays.asList(command.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(arglist);
        pb.inheritIO();
        try {
            Process process = pb.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
