import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Actividad1 {
    public static void main(String[] args) {

        ProcessBuilder pb = new ProcessBuilder(args);

        try {
            Process process = pb.start();
            process.waitFor(2, TimeUnit.SECONDS);
            InputStream input = process.getInputStream();
            if (!process.waitFor(2, TimeUnit.SECONDS)) {
                System.out.println("Tiempo de espera excedido");
            }else {

                /**
                 * Se tiene que comprobar si el proceso ha finalizado correctamente
                 * para mostrar la salida de error
                 */


                Scanner scanner = new Scanner(input);
                // pb.inheritIO(); Si se pone esta línea no es necesaria la siguiente y en todo 
                // caso, se pondria justo despues de Processbuilder.
                while (scanner.hasNext()){
                    System.out.println(scanner.nextLine());
                }
            }
        } catch (IOException ex) {
            System.out.println("ERROR!! con el proceso hijo.");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}